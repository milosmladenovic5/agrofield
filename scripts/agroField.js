var map;
var overlay;
var drawingManager;
var colorsArray = ['#C0C0C0', '#000000', '#FF0000', '#800000', '#F08080', '#FA8072', '#FFA07A', '#00FF00', '#FFFF00', '#808000',
                    '#00FF00', '#008000', '#00FFFF', '#008080', '#0000FF', '#FF00FF', '#800080', '#FFFFFF'];

var startDate='', endDate='', dateString='';

var ndviColor = '&color=0:912c00ff;0.11:eb6b25ff;0.2:d2ce0fff;0.3:f7fa15ff;0.4:35db39ff;0.46:37c020ff;0.57:34d019ff;0.73:1dbe32ff;1:376e06ff';
var eviColor = '&color=0:ffffffff;0.06:f7fcb9ff;0.18:3b00ebff;0.31:c840e7ff;0.48:df0003ff;0.62:d52121ff;0.67:d9e113ff;0.78:bfd540ff;1:4bc403ff';

var polylineRefs = [];
var parcelsArray = [], parcelsAreaArray = [];

var startMouseMove = false, finishDrawingClick = false;

var startPosition, currentPosition;
var lineRef, mouseMoveHandler, clickHandler;


// Initialize the map and the custom overlay.
function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 11,
        center: {
            lat: 45.32227,
            lng: 19.93469
        },
        mapTypeId: 'hybrid'
    });

    /* add drawing manager */
    drawingManager = new google.maps.drawing.DrawingManager({
        drawingMode: google.maps.drawing.OverlayType.HAND,
        drawingControl: true,
        drawingControlOptions: {
            position: google.maps.ControlPosition.TOP_CENTER,
            drawingModes: ['polygon','rectangle']
        }
    });
    drawingManager.setMap(map);

    /* add drawing manager event listeners */
    /* rectangle and polygon are used for drawing polygons for NDVI and EVI satellite images */
    google.maps.event.addListener(drawingManager, 'rectanglecomplete', function(rectangle){
        var start = rectangle.getBounds().getNorthEast();
        var end = rectangle.getBounds().getSouthWest();

        var rectanglePoints = [];
        rectanglePoints.push(new google.maps.LatLng({lat: start.lat(), lng: start.lng()}));
        rectanglePoints.push(new google.maps.LatLng({lat: end.lat(), lng: start.lng()}));
        rectanglePoints.push(new google.maps.LatLng({lat: end.lat(), lng: end.lng()}));
        rectanglePoints.push(new google.maps.LatLng({lat: start.lat(), lng: end.lng()}));
      
        var bounds = rectangle.getBounds();
        getResourceAndDraw(rectanglePoints, bounds, rectangle);
    });

    google.maps.event.addListener(drawingManager, 'polygoncomplete', function(polygon){
        var polygonPoints = polygon.getPath().getArray();

        var bounds = new google.maps.LatLngBounds(swPoint(polygonPoints), nePoint(polygonPoints));

        getResourceAndDraw(polygonPoints, bounds, polygon);
    });

    google.maps.event.addListener(drawingManager, 'polylinecomplete', function(polyline) {
        parcelsArray.push(new MyPolygon(polyline.getPath().getArray()));
        parcelsAreaArray.push(polyline.getPath());

        document.getElementById("doneParcBtn").disabled = false;
     });

    document.getElementById("doneParcBtn").disabled = true;
    document.getElementById("splitLineBtn").disabled = true;
}

/* function that calls ImageOverlay constructor, written to enable NDVI and EVI satellite images drawing on the map */
function drawOverlay(bounds, object, url){
    overlay = new ImageOverlay(bounds, url, map);
    object.setMap(null);
}

/* function that enables mouse handlers for drawing split line and handles it's logic */
function drawSplitLine(){
    document.getElementById("splitLineBtn").disabled = !document.getElementById('splitLineBtn').disabled;
    drawingManager.setOptions({drawingMode: google.maps.drawing.OverlayType.HAND});

    clickHandler = google.maps.event.addListener(map, 'click', function () {
        if(finishDrawingClick == false) { 
            finishDrawingClick = true;
            mouseMoveHandler = google.maps.event.addListener(map, 'mousemove', function (event) {
                if(!startMouseMove){
                    startMouseMove = true;
                    startPosition = event.latLng;
                }
                else //remove current line from the map 
                    lineRef.setMap(null); 
    
                currentPosition = event.latLng;
    
                coords = [startPosition, currentPosition];
    
                lineRef = new google.maps.Polyline({
                    path: coords,
                    geodesic: true,
                    strokeColor: '#FF0000',
                    strokeOpacity: 1.0,
                    strokeWeight: 2,
                    clickable: false
                  });
                
                lineRef.setMap(map);
            });         
        }
        else {
            document.getElementById("splitLineBtn").disabled = !document.getElementById('splitLineBtn').disabled;

            //first remove handlers for click and mousemove events so that later they can be added
            //when the button for drawing split line is clicked again  
            google.maps.event.removeListener(mouseMoveHandler);
            google.maps.event.removeListener(clickHandler);

            if(lineRef != undefined){
                parcelsArray = dividePolygons(parcelsArray, lineRef.getPath().getArray()[0], lineRef.getPath().getArray()[1]);
                parcelsAreaArray = [];
                parcelsAreaArray = drawPolygonsAndGetAreas(parcelsArray);
                //remove the drawn line from the map after the new parceling is completed
                lineRef.setMap(null);
                lineRef = null;
            }  
            //reset the state of the drawing events
            finishDrawingClick = false;
            startMouseMove = false;          

        }       
    });
}

function dividePolygons(parcArray, startPoint, endPoint){
    var result = [];

    for (var i=0; i<parcArray.length; i++){
        var intersections =  parcArray[i].linePolyIntersection(startPoint, endPoint);
        var newPolygons = parcArray[i].divide(intersections);

        if(newPolygons.constructor != Array){
            result.push(parcArray[i]);
            continue;
        }

        for(var j=0; j<newPolygons.length; j++)
            result.push(newPolygons[j]);
    }
    return result;
}

function drawPolygonsAndGetAreas(myPolygonArray){
    var parcelAreas = [];

    for (var i=0; i<myPolygonArray.length; i++){   
        var polygon = new google.maps.Polygon({
            paths: myPolygonArray[i].pointsArray,
            strokeColor: '#FF0000',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: colorsArray[i],
            fillOpacity: 0.35
        });

        var polygonArea = google.maps.geometry.spherical.computeArea(polygon.getPath())*0.0001;
        parcelAreas.push(polygonArea);

        //var listener = attachAreaInfoWindow(polygon, polygonArea);
        polygon.setMap(map);
    }
    return parcelAreas;
}
google.maps.event.addDomListener(window, 'load', initMap);

// function attachAreaInfoWindow(polygon, area) {
//     var infoWindow = new google.maps.InfoWindow();
//     var clickListener = google.maps.event.addListener(polygon, 'click', function (e) {
//         infoWindow.setContent(area+"ha");
//         var latLng = e.latLng;
//         infoWindow.setPosition(latLng);
//         infoWindow.open(map);
//     });
//     return clickListener;
// }
