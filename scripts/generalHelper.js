function urlBuilder(coordArray, colorMode, order, dateStr)
{
    var baseUrl = 'http://a.sat.owm.io/sql/15/?appid=38db99156e817077feafaef96a12ae54&overzoom=true'+ colorMode + order + dateStr + '&from=s2&polygon={%22type%22:%22Feature%22,%22properties%22:{},%22geometry%22:{%22type%22:%22Polygon%22,%22coordinates%22:[[';

    var coordString='';
    for (var i=0; i<coordArray.length; i++)
        coordString += '[' + coordArray[i].lng() + ',' + coordArray[i].lat() + '],';

    coordString += '[' + coordArray[0].lng() + ',' + coordArray[0].lat() + ']]]}}';

    return (baseUrl+coordString);
}

function checkAndGetColorMode()
{   
    var mode='', colorMode=''; 

    if(document.getElementById('ndviRd').checked)
    {
        mode="&op=ndvi";
        colorMode = ndviColor;
    }
    else
    {
        mode="&op=evi";
        colorMode = '&select=b8,b4,b2' + eviColor;
    }
    return colorMode + mode;
}

function checkAndGetOrder()
{
    var order = '&order=';

    if(document.getElementById('lastRd').checked)
        order+='last';
    else
        order+='best';

    return order;
}

function doneDrawingParcels(btnRef)
{
    btnRef.disabled = true;
    document.getElementById("startParcBtn").disabled = false;
    document.getElementById("splitLineBtn").disabled = false;
}

function startDrawingParcels(btnRef)
{
    drawingManager.setOptions({drawingMode: google.maps.drawing.OverlayType.POLYLINE});
    btnRef.disabled = true;
}

function getResourceAndDraw(pointsArray, bounds, object)
{
    var colorMode = checkAndGetColorMode();
    var order = checkAndGetOrder();
    checkDate();
    var url = urlBuilder(pointsArray, colorMode, order, dateString);

    $.get(url, function(data) {
        //console.log( "success sending request" );
      })
        .done(function() {
            drawOverlay(bounds, object, url);
        })
        .fail(function() {
          document.getElementById('errorLbl').innerText = "Set another date range";
          object.setMap(null);
        });
}

function checkDate()
{
    if(document.getElementById("startDate").value != '' & document.getElementById("endDate").value != '')
    {
        startDate = document.getElementById("startDate").value;
        endDate = document.getElementById("endDate").value;
        dateString = '&where=between('+ startDate + ':' + endDate + ')';
    }
}

function clearLbl()
{
    document.getElementById('errorLbl').innerText = '';

}
