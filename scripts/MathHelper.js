var Zero = 0.000000001;
var GeoTolerance = 0.0000001;

var Intersection = function (exist, overlapping, point){
    //Public properties
    this.exist = exist;
    this.overlapping = overlapping;
    this.point = point;
};

function dec(point1, point2)
{
    var x = point1.lat() - point2.lat();
    var y = point1.lng() - point2.lng();
    var retPoint = new google.maps.LatLng({lat: x, lng: y});
    return retPoint;
}

function cross(point1, point2)
{
    var magnitude = point1.lat() * point2.lng() - point1.lng() * point2.lat();
    return magnitude;
}

 /** Check if point inside line segment
     * dotproduct = (c.x - a.x) * (b.x - a.x) + (c.y - a.y)*(b.y - a.y)
     * squaredlengthba = (b.x - a.x)*(b.x - a.x) + (b.y - a.y)*(b.y - a.y)
     * @param start start of line segment
     * @param end end of line segment
     * @param another point for which we check if its inside line segment
     * @param tolerance tolerance
     * @return true if point inside line segment (if point == start || point == end return false!)
     */
function isPointBetween(start, end, another, tolerance){
    var a = start;
    var b = end;
    var c = another;

    var crossProduct = (c.lng() - a.lat()) * (b.lat() - a.lng()) - (c.lat() - a.lat()) * (b.lng() - a.lng());
    if (Math.abs(crossProduct) > tolerance) {
        return false;
    }
    var dotProduct = (c.lat() - a.lat()) * (b.lat() - a.lat()) + (c.lng() - a.lng()) * (b.lng() - a.lng());
    var squaredLengthBA  = (b.lat() - a.lat()) * (b.lat() - a.lat()) + (b.lng() - a.lng()) * (b.lng() - a.lng());
    return dotProduct  > 0 && dotProduct  < squaredLengthBA;
}

function areCollinear (start,  end,  middle, tolerance) {
    // You can check if the area of the ABC triangle is 0:
    // Of course, you don't actually need to divide by 2.
    var a = start;
    var b = end;
    var c = middle;
    var triangleArea = a.lat() * (b.lng() - c.lng()) + b.lat() * (c.lng() - a.lng()) + c.x * (a.lng() - b.lng());
    return Math.abs(triangleArea) < tolerance;
}

// p1, p2, q1, q2 = GeoPoints
function intersection( p1,  p2,  q1,  q2) {
    var res = new Intersection(false, false, null);

    var p = p1, r = dec(p2, p);
    var q = q1, s = dec(q2, q);

    var rxs = cross(r,s);
    var q_p = dec(q, p); // q - p
    // almost Zero
    if (Math.abs(rxs) < Zero) {
        // check if lines are collinear or not
        if (Math.abs(cross(q_p, r)) < Zero) {
            // TODO: for now it is not important if lines are disjoint or overlapping
            return res;
        } else {
            // line segments are parallel and do not intersect
            return res;
        }
    }
    var t = cross(q_p, s) / rxs;
    var u = cross(q_p, r) / rxs;
    // if line segments are intersect return point of intersection
    if (t >= 0 && t <= 1 && u >= 0 && u <= 1) {
        res.exist = true;
        res.point = new google.maps.LatLng({lat: p.lat() + t * r.lat(), lng: p.lng() + t * r.lng()});
        //console.log(res);
        return res;
    }

    //console.log(res);
    return res;
}

// parameters are  LatLng-s in JS
function getOrientation (pp,  qq,  rr) {
    var p = pp, q = qq, r = rr;
    var val = (q.lng() - p.lng()) * (r.lat() - q.lat()) - (q.lat() - p.lat()) * (r.lng() - q.lng());

    if (Math.abs(val) < Zero) {
        return 0;
    }
    return (val > 0) ? 1 : 2;
}

function getMiddle(latlngList) {
    var minX, maxX, minY, maxY;
    maxX = minX = latlngList[0].lat();
    maxY = minY = latlngList[0].lng();
    for (var i = 1; i < latlngList.length; i++) {
        var p = latlngList[i];
        maxX = Math.max(p.lat(), maxX);
        minX = Math.min(p.lat(), minX);
        maxY = Math.max(p.lng(), maxY);
        minY = Math.min(p.lng(), minY);
    }
    return new google.maps.LatLng({lat: minX + (maxX - minX) / 2, lng: minY + (maxY - minY) / 2});
}

function swPoint(cordArray)
{
    var minLong = Number.MAX_VALUE;
    var minLat = Number.MAX_VALUE;

    for (var i = 0; i < cordArray.length; i++) {

        if (cordArray[i].lng() < minLong) 
            minLong = cordArray[i].lng();

        if (cordArray[i].lat() < minLat) 
            minLat = cordArray[i].lat();
    }

    return (new google.maps.LatLng(minLat, minLong));
}

function nePoint(cordArray)
{
    var maxLong = Number.MIN_VALUE;
    var maxLat = Number.MIN_VALUE;
    
    for (var i = 0; i < cordArray.length; i++) {

        if (cordArray[i].lng() > maxLong) 
            maxLong = cordArray[i].lng();

        if (cordArray[i].lat() > maxLat) 
             maxLat = cordArray[i].lat();
    }

    return (new google.maps.LatLng(maxLat, maxLong));
}
