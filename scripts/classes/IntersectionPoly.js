var IntersectionPoly = function(point, polygonPointIndex)
{
    this.point = point;
    this.polygonPointIndex = polygonPointIndex;
    this.index = 0;
};

IntersectionPoly.prototype.setIndex = function(index)
{
    this.index = index;
};

IntersectionPoly.prototype.getIndex = function()
{
    return this.index;
};

IntersectionPoly.prototype.getPolygonPointIndex = function()
{
    return this.polygonPointIndex;
};

IntersectionPoly.prototype.getPoint = function()
{
    return this.point;
};