var MyPolygon = function(arrayPoints)
{
    if(arrayPoints === undefined)
        this.pointsArray = [];
    else
        this.pointsArray = arrayPoints;

    //this.pointsArray.push.apply(this.pointsArray, arrayPoints);
    this.CNV_LAT_LONG2UTM = true;
    this.FillColorSelected = 0x88EE9944;
    this.FillColorNormal = 0x88AA0000;
};

MyPolygon.prototype.add = function(latLngPoint)
{
    var lat = latLngPoint.lat(), lng = latLngPoint.lng();
    var len = this.pointsArray.length;

    if(len > 0 && (this.pointsArray[len-1].lat() == lat && this.pointsArray[len-1].lng() == lng))
    {
        return;
    }
    this.pointsArray.push(latLngPoint);
};

MyPolygon.prototype.remove = function(position) {
    if (position >= 0 && position < points.size()) {
        points.remove(position);
    }
}

MyPolygon.prototype.removeLast = function() {
    if (this.pointsArray.length > 0) {
        this.pointsArray.pop();
    }
}

MyPolygon.prototype.clear = function() {
    this.pointsArray.splice(0,this.pointsArray.length);
}

MyPolygon.prototype.hasAny = function() {
    return this.pointsArray.length > 0;
}

MyPolygon.prototype.hasMoreThan2 = function() {
    return this.pointsArray.length > 2;
}

MyPolygon.prototype.getPoints = function() {
    return this.pointsArray;
}

MyPolygon.prototype.getMiddle = function() {
    if (this.pointsArray.length < 1) {
        return null;
    }

    var sumX = 0;
    var sumY = 0;
    for (let i = 0; i < this.pointsArray.length; i++) {
        var p = this.arrayPoints[i];
        sumX += p.lat();
        sumY += p.lng();
    }
    return new google.maps.LatLng({lat:sumX / this.pointsArray.length, lng: sumY / this.pointsArray.length});
    // return MathHelper.getMiddle(points);
}

MyPolygon.prototype.setPoints = function(points) {
    this.pointsArray  = points;
}

/**
 * We must prevent self intersecting polygons
 * This is expensive so only last point is checked for stepsBack line segments down bellow
 * currently doesnt work for collinear points
 * @param stepsBack how many steps backward we check for intersection
 */

MyPolygon.prototype.fixSelfIntersection = function(stepsBack) {
    if (this.pointsArray.length < 4) {
        return;
    }

    var len = this.pointsArray.length - 1;
    var end = this.pointsArray[len];
    var start =  this.pointsArray[len - 1];
    for (let i = len - 3; i >= len - stepsBack - 2 && i >= 0; --i) {
        var p1 =  this.pointsArray[i];
        var p2 =  this.pointsArray[i + 1];
        let isctn = intersection(start, end, p1, p2);
        if (isctn.exist) {
            // first after intersection becomes n-1 and vice verse. Analog for others between
            var last = len - 1;
            var first = i + 1;
            for (var j = 0; j < (last - first + 1) / 2; ++j) {
                // swap
                var lower = this.pointsArray[first + j];
                var upper = this.pointsArray[last - j];
                this.pointsArray.set(last - j, lower);
                this.pointsArray.set(first + j, upper);
            }
            // move all points from intersection one position up
//                for (int j = len - 1; j >= i + 1; --j) {
//                    points.set(j + 1, points.get(j));
//                }
//                // new position for end point is i + 1
//                points.set(i + 1, end);
            break;
        }
    }
}

MyPolygon.prototype.isPointInside = function(gp) {
    var INF = 10000;

    if (this.pointsArray.length < 3) {
        return false;
    }

    var extreme = new google.maps.LatLng(INF, 19.93469);
    var count = 0, i = 0;
    do {
        var next = (i + 1) % this.pointsArray.length;
        var p1 = this.pointsArray[i];
        var p2 = this.pointsArray[next];
        var o = intersection(p1, p2, gp, extreme);
        if (o.exist)
        {
            if (getOrientation(p1, gp, p2) == 0) {
                return areCollinear(p1, p2, gp, GeoTolerance);
            }
            ++count;
        }
        i = next;
    } while (i != 0);

    return count % 2 == 1;
};

MyPolygon.prototype.linePolyIntersection = function(lineStart, lineEnd)
{
    let intersectionArray = [];

    if (this.pointsArray.length < 2)
        return intersectionArray;

    //check for 2 points first
    for (let i=0; i< this.pointsArray.length - 1; i++)
    {
        var o = intersection(this.pointsArray[i], this.pointsArray[i+1], lineStart, lineEnd);
        
        if(o.exist)
            intersectionArray.push(new IntersectionPoly(o.point, i));     
    }

    if(this.pointsArray.length > 2)
    {
        var end = this.pointsArray[0];
        var start = this.pointsArray[this.pointsArray.length-1];

        //console.log(lineStart, lineEnd);
        var o = intersection(start, end, lineStart, lineEnd);
        if(o.exist)
            intersectionArray.push(new IntersectionPoly(o.point, this.pointsArray.length-1));
    }

    //sort intersections along the line \
    if(Math.abs(lineStart.lng() - lineEnd.lng()) >= Math.abs(lineStart.lat() - lineEnd.lat()))
    {
        var orient = lineStart.lng() > lineEnd.lng() ? -1 : 1;
        intersectionArray.sort(function(a, b){
            var diff = a.getPoint().lng() - b.getPoint().lng();
            return (diff < 0 ? -1 : 1) * orient;
        });
    }
    else
    {
        var orient = lineStart.lat() > lineEnd.lat() ? -1 : 1;
        intersectionArray.sort(function(a, b){
            var diff = a.getPoint().lat() - b.getPoint().lat();
            return (diff < 0 ? -1 : 1) * orient;
        });
    }

    // if line segment start point is inside polygon remove first intersection
    if(this.isPointInside(lineStart) && intersectionArray.length >0)
    {
        intersectionArray.remove(0);
    }

    // same goes for end of line segment
    if (this.isPointInside(lineEnd) && intersectionArray.length > 0) {
        intersectionArray.splice(intersectionArray.length - 1, 1);
    }

    // update indexes for intersection points
    for (let i = 0; i < intersectionArray.length; i++) {
        intersectionArray[i].setIndex(i);
    }

    return intersectionArray;
};

/**
 * Divide polygon with intersection points
 Create an empty list of output polygons
Create an empty list of pending cross-backs (one for each polygon)
Find all intersections between the polygon and the line.
Sort them by position along the line.
Pair them up as alternating entry/exit lines.
Append a polygon to the output list and make it current.
Walk the polygon. For each edge:
Append the first point to the current polygon.
If there is an intersection with the split line:
    Add the intersection point to the current polygon.
    Find the intersection point in the intersection pairs.
    Set its partner as the cross-back point for the current polygon.
    If there is an existing polygon with a cross-back for this edge:
    Set the current polygon to be that polygon.
    Else
    Append a new polygon and new cross-back point to the output lists and make it current.
    Add the intersection point to the now current polygon.
* @param intersections intersection points (@seen intersection method)
* @return new polygons
*/

MyPolygon.prototype.divide = function(intersections) {
    // intersection count must be even because every in must have its out
    if (intersections.length < 2 || intersections.length % 2 != 0)
        return this;

    var res = [];
    // create hash map for easier access
    // key => polygon vertice
    // value => intersection index
    var hashMap = {};
    for (let i = 0; i < intersections.length; i++) {
        hashMap[intersections[i].getPolygonPointIndex()]=i;
    }

    // key => intersection point pair (0, 1) = 1, (2, 3) = 2
    // value => polygon which cross-back is that intersection point
    var crossBacks = {};

    var currentPolygon = new MyPolygon();
    res.push(currentPolygon);

    for (let i = 0; i < this.pointsArray.length; i++)
    {
        currentPolygon.pointsArray.push(this.pointsArray[i]);

        if (i in hashMap) {
            let int = intersections[hashMap[i]];
            currentPolygon.pointsArray.push(int.getPoint());
            // pairs are (0, 1), (2, 3), (3, 4)...
            var crossBackIndexPair = parseInt(int.getIndex() / 2); // + 1 - 2 * (in.getIndex() % 2);
            if (crossBackIndexPair in  crossBacks)
            {
                currentPolygon = crossBacks[crossBackIndexPair];
            }
            else
            {
                crossBacks[crossBackIndexPair]=currentPolygon;
                currentPolygon = new MyPolygon();
                res.push(currentPolygon);
                //crossBacks.put(crossBackIndexPair, currentPolygon);
            }
            currentPolygon.add(intersections[hashMap[i]].getPoint());
        }
    }
    //console.log(res);
    return res;
};

MyPolygon.prototype.getNewPos = function(pos, increment) {
    var newPos = pos + increment;

    if (newPos < 0){
        newPos = this.pointsArray.length + newPos;
    } else if (newPos >= this.pointsArray.length){
        newPos = newPos - this.pointsArray.length;
    }
    return newPos;
};

MyPolygon.prototype.getMiddle = function()
{
    if (this.pointsArray.length < 1) {
        return null;
    }

    var sumX = 0;
    var sumY = 0;
    for (let i = 0; i < this.pointsArray.length; i++) {
        var p = this.pointsArray[i];
        sumX += p.lat();
        sumY += p.lng();
    }
    return new google.maps.LatLng({lat: sumX / points.size(), lng: sumY / points.size()});
}